const render = require('./render')
const state = require('./state')

const breakWord = (chunk, word) => `${chunk.repeat(3)}${word}`.toUpperCase()

const miss = (chunk) => {
  if (state.trap) {
    state.trap.word = breakWord(chunk, state.trap.word)
  } else {
    state.visibles.forEach(visible => visible.word = breakWord(chunk, visible.word))
  }
  state.lives--
  if (state.lives < 0) {
    state.finish = true
  }
  render()
}

const abort = () => {
  state.aborted = true
  state.finish = true
  render()
  clearTimeout(state.timeout)
  process.exit(0)
}

module.exports = { miss, abort }
