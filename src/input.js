const state = require('./state')
const render = require('./render')
const { miss, abort } = require('./ending')

const input = () => {
  process.stdin.setEncoding('utf8')
  process.stdin.setRawMode(true)
  process.stdin.on('readable', () => {
    const chunk = process.stdin.read()
    if (chunk !== null) {
      if (chunk === '\n' || chunk === '\r' || chunk === '\u0004') {
        process.stdin.setRawMode(false)
        process.stdin.emit('end')
        abort()
      } else {
        state.trap = state.trap || state.visibles.find(visible => visible.word[0] === chunk)
        if (state.trap) {
          if (chunk === state.trap.word[0]) {
            if (state.trap.word.length === 1) {
              state.visibles.splice(state.visibles.indexOf(state.trap), 1)
              state.trap = null
            } else {
              state.trap.word = state.trap.word.slice(1)
              state.trap.isTrapped = true
            }
            render()
          } else {
            miss(chunk)
          }
        } else {
          miss(chunk)
        }
      }
    }
  })
}

module.exports = input
