const parse = text => text
  .replace(/['`"’]/g, '')
  .replace(/[\W_]+/g, ' ')
  .trim()
  .toLowerCase()
  .split(' ')

module.exports = parse

