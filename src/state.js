const state = {
  words: [],
  visibles: [],
  trap: null,
  lives: 3,
  timeout: null,
  aborted: false,
  finish: false,
}

module.exports = state
