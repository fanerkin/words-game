const pick = array => array.splice(Math.floor(array.length * Math.random()), 1)[0]

const shuffle = array => {
  const original = array.slice()
  const shuffled = []

  while (original.length) {
    shuffled.push(pick(original))
  }

  return shuffled
}

module.exports = shuffle

