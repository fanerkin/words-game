const state = require('./state')

const WIDTH = 80
const colors = ['31', '32', '33', '34', '35', '36', '37']
const MAX_WORDS = 20

const render = () => {
  process.stdout.write('\x1bc\x1b[3J')
  process.stdout.write(`\x1b[1;31m${'\u2665'.repeat(Math.max(state.lives, 0))}\x1b[0m\n`)
  process.stdout.write(`${state.visibles
    .map(({ word, padding, isTrapped }) => ({
      word: word.padStart(padding).padEnd(WIDTH),
      isTrapped,
    }))
    .map(({ word, isTrapped }) => isTrapped
      // use inverse for brighter colors
      ? `\x1b[1;7;40;${colors[Math.floor(colors.length * Math.random())]}m${word}\x1b[0m\n`
      : `\x1b[1;37;40m${word}\x1b[0m\n`
    )
    .reverse()
    .join('')
  }`)
  process.stdout.write(
    '.'
      .repeat(MAX_WORDS - state.visibles.length + 1)
      .split('.')
      .map(w => '\x1b[40m \x1b[0m'.repeat(WIDTH))
      .join('\n')
  )

  if (!state.finish) {
    return
  }
  if (state.aborted) {
    process.stdout.write('\n\x1b[1;34mGAME ABORTED\x1b[0m\n')
  } else if (
    state.lives < 0
    || state.visibles.length > MAX_WORDS
    || (!state.words.length && state.visibles.length)) {
    process.stdout.write('\n\x1b[1;31mGAME OVER\x1b[0m\n')
  } else if (!state.words.length && !state.visibles.length) {
    process.stdout.write('\n\x1b[1;36mYOU WIN\x1b[0m\n')
  }
}

module.exports = render
