const parse = require('./parse')
const shuffle = require('./shuffle')
const render = require('./render')
const text = require('../text').text.slice(0, 10)
const state = require('./state')

const DELAY = 2000
const WIDTH = 80
const MAX_WORDS = 20

const showWord = (word) => {
  state.visibles.push({
    word,
    padding: Math.max(Math.floor(WIDTH * Math.random()), word.length)
  })
  render()
}

const showDelayed = () => new Promise(resolve => {
  const f = () => {
    state.timeout = setTimeout(() => {
        if (state.words.length && !state.finish && state.visibles.length < MAX_WORDS) {
          showWord(state.words.shift())
          f()
        } else {
          resolve()
        }
    }, DELAY)
  }
  f()
})

const onFinish = () => {
  setTimeout(() => {
    state.finish = true
    render()
  }, 2000)
}

const restart = () => {
  setTimeout(output, 3000)
}

const output = () => {
  state.visibles.length = 0
  state.lives = 3
  state.trap = null
  state.words = shuffle(parse(text))
  state.aborted = false
  state.finish = false

  return showDelayed()
    .then(onFinish)
    .then(restart)
}

module.exports = output
